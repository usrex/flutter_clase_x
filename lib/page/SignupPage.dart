import 'package:flutter/material.dart';
import 'package:flutter_clase_x/data/Data_Helper.dart';
import 'package:flutter_clase_x/model/UserModel.dart';
import 'package:toast/toast.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  //instacia de la DB
  DataHelper db = DataHelper();
  //controller TextFiel
  var name = TextEditingController();
  var email = TextEditingController();
  var pass1 = TextEditingController();
  var pass2 = TextEditingController();
  //para el errortext
  bool _passError = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: _signUp()),
    );
  }

  Widget _signUp() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 35),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Sign UP",
            style: TextStyle(fontSize: 25),
          ),
          SizedBox(
            height: 25,
          ),
          TextField(
            controller: name,
            decoration: InputDecoration(labelText: "Nombre"),
            keyboardType: TextInputType.text,
          ),
          SizedBox(
            height: 25,
          ),
          TextField(
            controller: email,
            decoration: InputDecoration(labelText: "email"),
            keyboardType: TextInputType.emailAddress,
            
          ),
          SizedBox(
            height: 25,
          ),
          TextField(
            controller: pass1,
            decoration: InputDecoration(
              labelText: "contraseña",
              errorText: _passError ? null : "contraseñas distintas",
            ),
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
          ),
          SizedBox(
            height: 25,
          ),
          TextField(
            onChanged: (contrasenia2) {
              //cambiamos estados redibujamos el estado de los TextField
              setState(() {
                if (contrasenia2 == pass1.text.toString()) {
                  print("Correcto");
                  _passError = true;
                } else {
                  _passError = false;
                  print("error");
                }
              });
            },
            controller: pass2,
            decoration: InputDecoration(
                labelText: "repita contraseña",
                errorText: _passError ? null : "contraseñas distintas"),
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
          ),
          SizedBox(
            height: 25,
          ),
          RaisedButton(
            child: Text("Log In"),
            onPressed: () async{

              //mandamos a la sig vista despues de registrar los datos
              UserModel user = UserModel(
                name: name.text.toString().trim(),
                email: email.text.toString().trim(),
                pass: pass2.text.toString().trim());
                print(user);
              //lanzamos un toat
              Toast.show("Toast plugin app", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
              
              //mandamos datos a la DB para registrarlos
              int res = await db.insertUser(user);
              print(res);
              Navigator.pushNamed(context, '/Home',arguments: user);
            },
          ),
        ],
      ),
    );
  }
}
